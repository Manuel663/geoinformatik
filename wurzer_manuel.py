import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance(self, p2):
        dx = p2.x - self.x
        dy = self.y - p2.y
        euklDist = math.sqrt(dx**2 + dy**2)

    def area(self):
        return 0

class GeographicPoint(Point):
    def distance(self, p2):

        x1 = math.radians(self.x)
        x2 = math.radians(p2.x)
        y1 = math.radians(self.y)
        y2 = math.radians(p2.y)

        a = (math.sin((x1-x2)/2))**2 + math.cos(x1) * math.cos(x2) * (math.sin((y1-y2)/2))**2
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = 6371 * c

        return d


p1 = GeographicPoint(53.548454615468394, 9.997147561461814) # Hamburg
p2 = GeographicPoint(52.50520139993045, 13.389804922191308) # Berlin

#print(p1.distance(p2))
print(help(GeographicPoint)) # Zeigen dass vererbt
